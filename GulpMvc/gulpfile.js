﻿var gulp = require('gulp');
var concat = require('gulp-concat');
var minifyCss = require('gulp-minify-css');

var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

gulp.task('app-styles', function () {

    return gulp.src(['./Content/**/*.css'])
      .pipe(minifyCss())
      .pipe(concat('app.min.css'))
      .pipe(gulp.dest('app/'))
      .pipe(reload({stream: true}));
});

gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: "localhost:50000"
    });
});

gulp.task('default', ['app-styles', 'browser-sync'], function () {
    gulp.watch("./Content/**/*.css", ['app-styles']);
});