﻿using System.Web;
using System.Web.Optimization;

namespace GulpMvc
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/scripts/vendor").Include(
                        "~/Scripts/require.js",
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/scripts/app").Include(
                        "~/Scripts/Main/app.js",
                        "~/Scripts/Second/app.js"));

            bundles.Add(new StyleBundle("~/css/vendor").Include(
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/css/app").Include(
                      "~/Content/app.css"));
        }
    }
}
