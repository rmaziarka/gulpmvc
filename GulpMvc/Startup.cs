﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GulpMvc.Startup))]
namespace GulpMvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
